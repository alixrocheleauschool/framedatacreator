﻿(function () {
    // Prevent emtpy 'a' and 'submit' btns
    // Left here as precaution in case we forget any links/forms in electron
    const targets = document.querySelectorAll('[href="#"], [type="submit"]');

    for (const element of targets) {
        element.addEventListener('click', event => {
            event.preventDefault();
        });
    }

    // Initialize popovers
    const popoverElements = document.querySelectorAll('[data-bs-toggle="popover"]');
    for (const popover of popoverElements) {
        new bootstrap.Popover(popover);
    }

    // Initialize tooltips
    const tooltipElements = document.querySelectorAll('[data-bs-toggle="tooltip"]');
    for (const tooltip of tooltipElements) {
        new bootstrap.Tooltip(tooltip);
    }

    // Init Image containers to scroll to about 50%
    function ScrollImage(valueX, valueY) {
        let columnHeight = $('.image-overflow:visible').parent().parent().height();
        let columnWidth = $('.image-overflow:visible').parent().parent().parent().width();

        $('.image-overflow').scrollLeft(valueX + 960 - columnWidth / 2);
        $('.image-overflow').scrollTop(valueY + 540 - columnHeight / 2);
    }
    ScrollImage(0, 0);
    //Header Nav
    function ShowTab(tab) {
        const hideLayouts = $('div.content');
        hideLayouts.addClass('invisible');
        const element = $('#' + tab);
        if (element.hasClass('invisible')) {
            element.removeClass('invisible');
        }
        /*ScrollImage(0, 0);*/
    }
    const navitems = $('ul.nav-tabs li a');
    for (const element of navitems) {
        element.addEventListener('click', function () {
            ShowTab(element.dataset.nav);
        });
    }
    //end header nav

    // Rezise and split functions (for layouts)
    let x = 0;
    let y = 0;
    let resizers = $('.resizer');
    let resizer = resizers[0];
    let prevSibling = resizer.previousElementSibling;
    let nextSibling = resizer.nextElementSibling;

    let prevSiblingWidth = 0;
    let prevSiblingHeight = 0;
    // Handle the mousedown event triggered when user drags the resizer
    const mouseDownHandler = function (e) {
        resizer = e.target;
        prevSibling = resizer.previousElementSibling;
        nextSibling = resizer.nextElementSibling;
        x = e.clientX;
        y = e.clientY;
        prevSiblingWidth = prevSibling.getBoundingClientRect().width;
        prevSiblingHeight = prevSibling.getBoundingClientRect().height;
        document.addEventListener('mousemove', mouseMoveHandler);
        document.addEventListener('mouseup', mouseUpHandler);
    };
    const mouseMoveHandler = function (e) {
        const direction = resizer.dataset.direction;
        // How far the mouse has been moved
        const dx = e.clientX - x;
        const dy = e.clientY - y;
        switch (direction) {
            case 'vertical':
                const h = (prevSiblingHeight + dy) * 100 / resizer.parentNode.getBoundingClientRect().height;
                prevSibling.style.height = `${h}%`;
                break;
            case 'horizontal':
            default:
                const w = (prevSiblingWidth + dx) * 100 / resizer.parentNode.getBoundingClientRect().width;
                prevSibling.style.width = `${w}%`;
                break;
        }

        const cursor = direction === 'horizontal' ? 'col-resize' : 'row-resize';
        resizer.style.cursor = cursor;
        document.body.style.cursor = cursor;

        prevSibling.style.userSelect = 'none';
        prevSibling.style.pointerEvents = 'none';

        nextSibling.style.userSelect = 'none';
        nextSibling.style.pointerEvents = 'none';
    };
    const mouseUpHandler = function () {
        resizer.style.removeProperty('cursor');
        document.body.style.removeProperty('cursor');

        prevSibling.style.removeProperty('user-select');
        prevSibling.style.removeProperty('pointer-events');

        nextSibling.style.removeProperty('user-select');
        nextSibling.style.removeProperty('pointer-events');

        // Remove the handlers of `mousemove` and `mouseup`
        document.removeEventListener('mousemove', mouseMoveHandler);
        document.removeEventListener('mouseup', mouseUpHandler);
    };
    // Attach the handler
    for (const element of resizers) {
        element.addEventListener('mousedown', mouseDownHandler);
    }
    //End resize and split

    //New project menu    
    const newProjectDialog = $("#dialog-new-project").dialog({
        dialogClass: "ui-dialog-hide-handler",
        position: {
            my: "center top",
            at: "left+25% top+10%",
            of: window
            },
       autoOpen: false,       
        height: 600,
        width: 400,
        modal: true,
        buttons: {
            "Create": function () {
                createNewProject();
                newProjectDialog.dialog("close");
            },
            Cancel: function () {
                newProjectDialog.dialog("close");
            }
        },
        close: function () {
       },
       resizable: true,
       classes: {
           "ui-dialog": "card ui-dialog-window",
           "ui-dialog-titlebar-close": "btn btn-outline-header-menu",
           "ui-dialog-buttonpane": "card-footer",
           "ui-dialog-titlebar":"card-header",
       },
       create: function(event, ui) {
           $(event.target).parent().css('position', 'absolute');
           var widget = $(this).dialog("widget");
           $(".ui-dialog-titlebar-close", widget)
               .addClass("ui-icon-close");
       }       
   });

    $('.ui-dialog-buttonset button').addClass('btn btn-outline-header-menu');

    
    function createNewProject() {       
        window.newProjectDialogDotNet.invokeMethodAsync('JsCreateProject');
    }

    $("#create-project").button().on("click", function () {
        newProjectDialog.dialog("open");
    });
    //End new project menu

    //Open preferences menu
    const openPreferenceDialog = $("#dialog-preferences").dialog({
        dialogClass: "ui-dialog-hide-handler",
        position: {
            my: "center top",
            at: "left+25% top+10%",
            of: window
        },
        autoOpen: false,
        height: 600,
        width: 600,
        modal: true,
        buttons: {
            "Close": function () {
                openPreferenceDialog.dialog("close");
            }
        },
        close: function (event, ui) {
            closePreferences();
        },
        resizable: true,
        classes: {
            "ui-dialog": "card ui-dialog-window",
            "ui-dialog-titlebar-close": "btn btn-outline-header-menu",
            "ui-dialog-buttonpane": "card-footer",
            "ui-dialog-titlebar": "card-header",
        },
        create: function (event, ui) {
            $(event.target).parent().css('position', 'absolute');
            var widget = $(this).dialog("widget");
            $(".ui-dialog-titlebar-close", widget)
                .addClass("ui-icon-close");
        }
    });

    $('.ui-dialog-buttonset button').addClass('btn btn-outline-header-menu');
    
    function closePreferences() {
        window.preferenceDotNet.invokeMethodAsync('JsClosePreferences');
    }
    $("#open-preferences-window").button().on("click", function () {
        openPreferenceDialog.dialog("open");
    });    
    //end preferences menu

    //Add special listeners to help manage the hitbox handles for resizing
    $('.image-image').on('mouseup', function (e) {
        var resizable = $(e.target).closest('.hitbox');
        $('.hitbox').removeClass('ui-focus-hitbox');
        if (resizable.length) {
            $(resizable).addClass('ui-focus-hitbox');
        }
    });
    $('.ui-resizable-handle').on('mouseup', function (e) {
        var resizable = $(e.target).closest('.hitbox');
        $('.hitbox').removeClass('ui-focus-hitbox');
        if (resizable.length) {
            $(resizable).addClass('ui-focus-hitbox');
        }
    });
    $('#inside').on('mouseup', function (e) {
        var resizable = $(e.target).closest('.hitbox');
        $('.hitbox').removeClass('ui-focus-hitbox');
        if (resizable.length) {
            $(resizable).addClass('ui-focus-hitbox');
        }
    });
}
)
    ();

