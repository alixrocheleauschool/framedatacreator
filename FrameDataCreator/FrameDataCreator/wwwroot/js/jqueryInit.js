﻿//-----This is to delete the ref from Electron's node require which will block jquery

(function () {   
    window.nodeRequire = require;
    delete window.require;
    delete window.exports;
    delete window.module;
})();