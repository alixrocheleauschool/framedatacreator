﻿namespace FrameDataCreator.Constants
{
    /// <summary>
    /// Static Definitions
    /// </summary>
    public static class SD
    {
        //View Areas in the page layout
        public const int BlankView = 0;
        public const int TreeView = 1;
        public const int ImageView = 2;
        public const int AnimationView = 3;
        public const int PropertiesView = 4;
        public const int SpriteView = 5;
        public const int TemplateTreeView = 6;
        public const int TemplatePropertiesView = 7;

        //Attribute type
        public const int Animation = 1;
        public const int FrameImage = 2;
        public const int HitBox = 3;
        public const int Integer = 4;
        public const int ListChoice = 5;
        public const int ListFromGroup = 6;
        public const int Origin = 7;
        public const int RadioChoice = 8;
        public const int Real = 9;
        public const int Text = 10;
        public const int BoolCheck = 11;

        //Preference type to display inputs
        public const int PrefBool = 0;
        public const int PrefInt = 1;
        public const int PrefPrefab = 99;

        //onion prefs not yet prefs
        public const int OnionNumber = 2;
        public const double OnionOpacity = 0.3;

    }
}
