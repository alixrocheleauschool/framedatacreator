﻿using Newtonsoft.Json;

namespace FrameDataCreator.Models
{
    /// <summary>
    /// Layer of data
    /// </summary>
    public class Layer : Template
    {
        /// <summary>
        /// Basic Constructor
        /// </summary>
        public Layer()
        {
            OriginalTemplateId = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// Constructor with a given template id
        /// </summary>
        /// <param name="templateId">The template Id to create the layer from</param>
        public Layer(string templateId)
        {

            OriginalTemplateId = templateId;
        }
        /// <summary>
        /// Add child to this layer and assign this layer as the child's parent
        /// </summary>
        /// <param name="layer">New layer to add to children</param>
        public void AddChild(Layer layer)
        {
            layer.Parent = this;
            layer.ParentId = Id;
            Children.Add(layer);
        }
        public bool Export { get; set; } = true;
        /// <summary>
        /// An Id related to something meaningful in your game
        /// </summary>
        public override string GameId { get; set; } = "";
        /// <summary>
        /// List of children layers
        /// </summary>
        public List<Layer> Children { get; set; } = new List<Layer>();
        /// <summary>
        /// Reference to this layer's parent
        /// </summary>
        [JsonIgnore]
        public Layer? Parent { get; set; } = null;
        /// <summary>
        /// This layer's parent Id
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// The original template id that this layer was created from
        /// </summary>
        public string OriginalTemplateId { get; set; }
        public List<Layer> GetFilteredLayers(string layerName)
        {
            var result = new List<Layer>();
            {
                if (Name.ToLower().Contains(layerName.ToLower()))
                {
                    result.Add(this);
                }
                foreach (var childLayer in Children)
                {
                    var childResult = childLayer.GetFilteredLayers(layerName);
                    result.AddRange(childResult);
                }
                return result;
            }
        }
        public Layer GetCloned()
        {
            Layer result = new();
            result.OriginalTemplateId = OriginalTemplateId;
            result.GameId = GameId;
            result.Name = Name;
            foreach (var attr in Attributes)
            {
                result.Attributes.Add(attr.Clone());
            }
            foreach (var child in Children)
            {
                result.AddChild(child.GetCloned());
            }

            return result;
        }
    }
}
