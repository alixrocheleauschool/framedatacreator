﻿namespace FrameDataCreator.Models
{
    /// <summary>
    /// Project prefab to choose from when creating new projects
    /// </summary>
    public class ProjectPrefab
    {
        /// <summary>
        /// Name of the prefab
        /// </summary>
        public string Name { get; set; } = "New Prefab";
        /// <summary>
        /// Json string of the data to create the project with
        /// </summary>
        public string Json { get; set; } = "";
    }
}
