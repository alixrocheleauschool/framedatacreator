﻿
using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;


namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Origin attribute of the frame (should be used in frame)
    /// </summary>
    public class Origin : IAttribute
    {
        private Position _default;
        private Position _value;
        /// <summary>
        /// Constructor default
        /// </summary>
        public Origin()
        {
            _default = new Position();
            _value = new Position();
            Name = "Origin";
        }
        /// <summary>
        /// Constructor with values
        /// </summary>
        /// <param name="default">The default position</param>
        /// <param name="value">The current position</param>
        public Origin(Position @default, Position value)
        {
            _default = @default ?? new Position();
            _value = value ?? new Position();
            Name = "Origin";
        }
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = (Position)value; }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = (Position)value; }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.Origin;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New Origin";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new Origin(
                new Position(_default.X, _default.Y),
                new Position(_value.X, _value.Y)
                );
            clone.Name = Name;
            clone.Id = Id;
            return clone;
        }
    }
}
