﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;
using System.Drawing;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// A Frame with an image
    /// </summary>
    public class FrameImage :  IAttribute
    {
        private int _default=0;
        private int _value=0;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="path">Image filepath</param>
        /// <param name="index">index</param>
        /// <param name="duration">duration in frames</param>
        public FrameImage(string path="",int index=0, int duration=1) 
        {
            ImagePath= path;
            Index= index;
            Duration= duration;
            Name = "New Frame";
        }
        /// <summary>
        /// Index of the frame
        /// </summary>
        public int Index { get => _value; set => _value= value; }
        /// <summary>
        /// Duration of the frame
        /// </summary>
        public int Duration { get; set; }
        /// <summary>
        /// File path to the image file to display (not exported)
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = Convert.ToInt32(value); }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = Convert.ToInt32(value); }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.FrameImage;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New FrameImage";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new FrameImage(ImagePath,Index, Duration);
            clone.Default = Default;
            clone.Value = Value;
            clone.Id = Id;
            clone.Name = Name;
            return clone;
        }
    }
}
