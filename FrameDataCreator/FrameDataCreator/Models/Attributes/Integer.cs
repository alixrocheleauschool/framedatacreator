﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Integer attribute
    /// </summary>
    public class Integer : Number, IAttribute
    {
        private int _default = 0;
        private int _value=0;

        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = Convert.ToInt32(value); }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = Convert.ToInt32(value); }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.Integer;
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone= new Integer();
            clone.Default = Default;
            clone.Value = Value;
            clone.Id = Id;
            clone.Name = Name;
            return clone;
        }
    }
}
