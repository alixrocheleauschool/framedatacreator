﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Real number attribute
    /// </summary>
    public class Real : Number, IAttribute
    {
        private decimal _value;
        private decimal _default;
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = Convert.ToDecimal(value); }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = Convert.ToDecimal(value); }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.Real;
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new Real();
            clone.Name = Name;
            clone.Default = Default;
            clone.Id = Id;
            clone.Value = Value;
            return clone;
        }
    }
}
