﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Common;

namespace FrameDataCreator.Models.Attributes
{
    /// <summary>
    /// Proposes a select list from all instances of a layer GameId
    /// </summary>
    public class ListFromGroup :  IAttribute
    {
        private string _default="";
        private string _value="";

        /// <summary>
        /// Game Id to search for to build the list
        /// </summary>
        public string LayerGameId { get; set; } = "GameId";
        /// <summary>
        /// How many levels to go up in the tree to build the list
        /// </summary>
        public int LayerParentLevel { get; set; } = 1;
        /// <summary>
        /// Default value
        /// </summary>
        public object Default { get => _default; set => _default = (String)value; }
        /// <summary>
        /// Curret value
        /// </summary>
        public object Value { get => _value; set => _value = (String)value; }
        /// <summary>
        /// Type of the attribute
        /// </summary>
        public int PropertyType { get; } = SD.ListFromGroup;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New List Group";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Clone an attribute as deep copy
        /// </summary>
        /// <returns>A deep copy of the attribute</returns>
        public IAttribute Clone()
        {
            var clone = new ListFromGroup();
            clone.Default = Default;
            clone.Value = Value;
            clone.Id = Id;
            clone.Name=Name;
            clone.LayerGameId = LayerGameId;
            clone.LayerParentLevel = LayerParentLevel;
            return clone;
        }
    }
}
