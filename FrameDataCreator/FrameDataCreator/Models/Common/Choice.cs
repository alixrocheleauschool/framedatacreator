﻿namespace FrameDataCreator.Models.Common
{
    /// <summary>
    /// A choice available in an attribute
    /// </summary>
    public class Choice
    {
        /// <summary>
        /// Constructor with key/value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public Choice(string key,string value) {
            NameValuePair=new KeyValuePair<string, string>(key,value);
        }
        /// <summary>
        /// Blank constructor
        /// </summary>
        public Choice() { }
        /// <summary>
        /// A key value pair of the Name of the choice and its value
        /// </summary>
        public KeyValuePair<string,string> NameValuePair { get; set; }
    }
}
