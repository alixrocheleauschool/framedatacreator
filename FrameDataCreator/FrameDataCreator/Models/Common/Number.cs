﻿namespace FrameDataCreator.Models.Common
{
    /// <summary>
    /// Number class for attributes
    /// </summary>
    public class Number : IIdentifier
    {
        /// <summary>
        /// Minimum
        /// </summary>
        public decimal Min { get; set; } = 0;
        /// <summary>
        /// Maximum
        /// </summary>
        public decimal Max { get; set; } = 1;
        /// <summary>
        /// Amount to increment by
        /// </summary>
        public decimal Increment { get; set; } = 0.1M;
        /// <summary>
        /// Name of attribute
        /// </summary>
        public string Name { get; set; } = "New Name";
        /// <summary>
        /// Guid
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString();
    }
}
