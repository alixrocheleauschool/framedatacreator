﻿namespace FrameDataCreator.Models.Common
{
    /// <summary>
    /// Attribute interface
    /// </summary>
    public interface IAttribute : IIdentifier
    {
        /// <summary>
        /// Default value of attribute
        /// </summary>
        public object Default { get; set; }
        /// <summary>
        /// Current value of attribute
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// type of the attribute
        /// </summary>
        public int PropertyType { get; }
        /// <summary>
        /// Clone method 
        /// </summary>
        /// <returns>A copy of the attribute</returns>
        public IAttribute Clone();
    }
}
