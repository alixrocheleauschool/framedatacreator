﻿namespace FrameDataCreator.Service
{
    /// <summary>
    /// Interface DI Service that refreshes blazor components
    /// </summary>
    public interface IRefreshService
    {
        /// <summary>
        /// List of refresh calls from blazor views
        /// </summary>
        event Action RefreshRequested;
        /// <summary>
        /// Invoke all refresh calls registered
        /// </summary>
        void CallRequestRefresh();
    }
    /// <summary>
    /// Service that refreshes blazor components
    /// </summary>
    public class RefreshService : IRefreshService
    {
        /// <summary>
        /// List of refresh calls from blazor views
        /// </summary>
        public event Action RefreshRequested;
        /// <summary>
        /// Invoke all refresh calls registered
        /// </summary>
        public void CallRequestRefresh()
        {
            RefreshRequested?.Invoke();
        }
    }
}
