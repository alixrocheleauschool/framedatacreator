﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models.Attributes;
using FrameDataCreator.Test.Fakes;

namespace FrameDataCreator.Test.Controllers
{
    [TestClass]
    public class ApplicationControllerTest
    {
        private FDCStateFake FDCStateFake { get; set; }
        public ApplicationControllerTest()
        {
            FDCStateFake = new FDCStateFake(ConfigurationFake.getConfig());
        }
        [TestInitialize()]
        public void Startup()
        {
            FDCStateFake = new FDCStateFake(ConfigurationFake.getConfig());
        }
        //[TestCleanup()]
        //public void Cleanup()
        //{
        //    FDCStateFake = new FDCStateFake(ConfigurationFake.getConfig());
        //}
        [TestMethod]
        public void GetProject_AssignProject_CurrentProjectEqualsStateProject()
        {
            //Arrange
            var appController = new ApplicationController(FDCStateFake);

            //Act
            var result = appController.GetProject();

            //Assert
            Assert.IsTrue(result == FDCStateFake.Project);
        }
        [TestMethod]
        public void GetTemplates_AssignTemplates_CurrentTemplatesEqualsStateTemplates()
        {
            //Arrange
            var appController = new ApplicationController(FDCStateFake);
            _ = appController.GetProject();
            //Act
            var result = appController.GetTemplates();

            //Assert
            Assert.IsTrue(result == FDCStateFake.Project.Templates);
        }
        [TestMethod]
        public void GetListFromLayer_ListCountEqualsStateCount()
        {

            //Arrange
            var appController = new ApplicationController(FDCStateFake);
            int expectedCount = FDCStateFake.Project.Layers.First().Children.First().Children.Count(); // collection, characters, ..n char

            _ = appController.GetProject();
            //Act
            var result = appController.GetListFromLayer("xx_characters").Count;

            //Assert
            Assert.IsTrue(result == expectedCount);
        }
        [TestMethod]
        public void GetPrefabs_ListCountEqualsStateCount()
        {
            //Arrange
            var appController = new ApplicationController(FDCStateFake);
            int expectedCount = FDCStateFake.Prefabs.Count;

            _ = appController.GetProject();
            //Act
            var result = appController.GetPrefabs().Count;

            //Assert
            Assert.IsTrue(result == expectedCount);
        }
        [TestMethod]
        public void GetLayerContext_ContextNotNull_LayerIsMissingAttributes_AttributesFromTemplateAreAdded()
        {
            //Arrange
            var appController = new ApplicationController(FDCStateFake);
            _ = appController.GetProject();
            var characterLayer = FDCStateFake.Project.Layers.First().Children.First().Children.First(); //First character
            appController.SetLayerContext(characterLayer.Id);

            //Add an attribute to template
            FDCStateFake.Project.Templates.First(t => t.Name == "Character").Attributes.Add(new Integer() { Name = "HpTest42" });

            //Act
            var result = appController.GetLayerContext();

            //Assert
            Assert.IsTrue(result?.Attributes.FirstOrDefault(a => a.Name == "HpTest42") != null);
        }
        [TestMethod]
        public void SetLayerContext_ContextHasFrameImage_ValidateImageContext()
        {
            //Arrange
            var appController = new ApplicationController(FDCStateFake);
            _ = appController.GetProject();
            var imageLayer = FDCStateFake.Project.Layers.Last().Children.Last().Children.First().Children.First().Children.First(); //First anim first frame
            appController.SetLayerContext(imageLayer.Id);

            //Act
            var result = appController.GetLayerContext();

            //Assert
            Assert.IsTrue(appController.GetImageContext() != null);
            Assert.IsTrue(appController.GetImageContext() == imageLayer.FindAttribute<FrameImage>(SD.FrameImage));
        }
        [TestMethod]
        public void RenameLayersAttributeFromTemplate_ValidateLayerAttributeName()
        {
            //Arrange
            var appController = new ApplicationController(FDCStateFake);
            _ = appController.GetProject();
            var template = FDCStateFake.Project.Templates.First(t => t.Name == "Animation");
            template.Attributes.Add(new Integer() { Name = "TestName" });
            var aninLayer = FDCStateFake.Project.Layers.First().Children.First().Children.First().Children.First().Children.First().Children.First(); //First anim 
            appController.SetLayerContext(aninLayer.Id);
            appController.RenameLayersAttributeFromTemplate(template);

            //Act
            var result = appController.GetLayerContext();
            template.Attributes.First().Name = "TestRename";
            appController.RenameLayersAttributeFromTemplate(template);
            result = appController.GetLayerContext();

            //Assert
            Assert.IsTrue("TestRename" == result?.Attributes.First().Name);
        }

    }
}
