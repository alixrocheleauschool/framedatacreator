﻿using FrameDataCreator.Constants;
using FrameDataCreator.Models;
using FrameDataCreator.Models.Attributes;
using Microsoft.Extensions.Configuration;

namespace FrameDataCreator.Test.Fakes
{
    public class FDCStateFake: FDCState
    {
        public FDCStateFake(IConfiguration configuration) :base(configuration)
        {
            Start("fakefdcsettings.json");
            GenerateFakeStateData();
        }

        private void GenerateFakeStateData()
        {
            NewProject("Test Project","Base",false);

            var stateId = Project.Templates.First(t => t.Name == "State").Id;
            var animId = Project.Templates.First(t => t.Name == "Animation").Id;
            var frameId = Project.Templates.First(t => t.Name == "Frame").Id;
            var hitbox = Project.Templates.First(t => t.Name == "HitBox");
            var hurtbox = Project.Templates.First(t => t.Name == "HurtBox");

            Project.Layers.First().Children.First().GameId = "xx_characters";

            var cyclops = new Layer(Project.Templates.First(t => t.Name == "Character").Id) { Name = "Cyclops" };
            var magneto = new Layer(Project.Templates.First(t => t.Name == "Character").Id) { Name = "Magneto" };
            #region state anim frame
            var stateStand = new Layer(stateId) { GameId = "stand" };
            var stateIdle=(new Layer(stateId) { GameId = "idle" });
            var anim1 = new Layer(animId) { Name = "anim1" };
            var anim2 = new Layer(animId) { Name = "anim2" };

            var frame1 = new Layer(frameId) { Name = "frame1" , Attributes=new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            var frame2 = new Layer(frameId) { Name = "frame2", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            var frame3 = new Layer(frameId) { Name = "frame3", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };

            var frame1_2 = new Layer(frameId) { Name = "frame1", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            var frame2_2 = new Layer(frameId) { Name = "frame2", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            var frame3_2 = new Layer(frameId) { Name = "frame3", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            #endregion
            #region Hitboxes
            var hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            var hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            var hitbox1= hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            var hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame2.Attributes.Add(hurtbox1);
            frame2.Attributes.Add(hurtbox2);
            frame2.Attributes.Add(hurtbox1);
            frame2.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame1.Attributes.Add(hurtbox1);
            frame1.Attributes.Add(hurtbox2);
            frame1.Attributes.Add(hurtbox1);
            frame1.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame3.Attributes.Add(hurtbox1);
            frame3.Attributes.Add(hurtbox2);
            frame3.Attributes.Add(hurtbox1);
            frame3.Attributes.Add(hurtbox2);

            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame2_2.Attributes.Add(hurtbox1);
            frame2_2.Attributes.Add(hurtbox2);
            frame2_2.Attributes.Add(hurtbox1);
            frame2_2.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame1_2.Attributes.Add(hurtbox1);
            frame1_2.Attributes.Add(hurtbox2);
            frame1_2.Attributes.Add(hurtbox1);
            frame1_2.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame3_2.Attributes.Add(hurtbox1);
            frame3_2.Attributes.Add(hurtbox2);
            frame3_2.Attributes.Add(hurtbox1);
            frame3_2.Attributes.Add(hurtbox2);
            #endregion
            #region add childs
            anim2.AddChild(frame1_2);
            anim2.AddChild(frame2_2);
            anim2.AddChild(frame3_2);
            anim1.AddChild(frame1);
            anim1.AddChild(frame2);
            anim1.AddChild(frame3);
            stateStand.AddChild(anim1);
            stateIdle.AddChild(anim2);
            cyclops.AddChild(stateIdle);
            cyclops.AddChild(stateStand);
            #endregion
            Project.Layers.First().AddChild(cyclops);

            #region state anim frame
            stateStand = new Layer(stateId) { GameId = "stand" };
            stateIdle = (new Layer(stateId) { GameId = "idle" });
            anim1 = new Layer(animId) { Name = "anim1" };
            anim2 = new Layer(animId) { Name = "anim2" };

            frame1 = new Layer(frameId) { Name = "frame1", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            frame2 = new Layer(frameId) { Name = "frame2", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            frame3 = new Layer(frameId) { Name = "frame3", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };

            frame1_2 = new Layer(frameId) { Name = "frame1", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            frame2_2 = new Layer(frameId) { Name = "frame2", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            frame3_2 = new Layer(frameId) { Name = "frame3", Attributes = new List<Models.Common.IAttribute>() { new FrameImage() { } } };
            #endregion
            #region Hitboxes
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame2.Attributes.Add(hurtbox1);
            frame2.Attributes.Add(hurtbox2);
            frame2.Attributes.Add(hurtbox1);
            frame2.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame1.Attributes.Add(hurtbox1);
            frame1.Attributes.Add(hurtbox2);
            frame1.Attributes.Add(hurtbox1);
            frame1.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame3.Attributes.Add(hurtbox1);
            frame3.Attributes.Add(hurtbox2);
            frame3.Attributes.Add(hurtbox1);
            frame3.Attributes.Add(hurtbox2);

            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame2_2.Attributes.Add(hurtbox1);
            frame2_2.Attributes.Add(hurtbox2);
            frame2_2.Attributes.Add(hurtbox1);
            frame2_2.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame1_2.Attributes.Add(hurtbox1);
            frame1_2.Attributes.Add(hurtbox2);
            frame1_2.Attributes.Add(hurtbox1);
            frame1_2.Attributes.Add(hurtbox2);
            hurtbox1 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hurtbox2 = hitbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox1 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            hitbox2 = hurtbox.FindAttribute<HitBox>(SD.HitBox)?.Clone();
            frame3_2.Attributes.Add(hurtbox1);
            frame3_2.Attributes.Add(hurtbox2);
            frame3_2.Attributes.Add(hurtbox1);
            frame3_2.Attributes.Add(hurtbox2);
            #endregion
            #region add childs
            anim2.AddChild(frame1_2);
            anim2.AddChild(frame2_2);
            anim2.AddChild(frame3_2);
            anim1.AddChild(frame1);
            anim1.AddChild(frame2);
            anim1.AddChild(frame3);
            stateStand.AddChild(anim1);
            stateIdle.AddChild(anim2);
            magneto.AddChild(stateIdle);
            magneto.AddChild(stateStand);
            #endregion
            Project.Layers.First().AddChild(magneto);
        }
    }
}