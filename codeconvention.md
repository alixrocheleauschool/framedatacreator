# Convention for C#


## Quick Links

| Syntax    |
| ----------- | 
| [Class Names](#class-names)        | 
| [Variable Names](#variable-names)        | 
| [Identifiers](#identifiers)        | 
| [Constants](#constants)        | 
| [Abbreviations](#abbreviations)        | 
| [Abbreviation Casing](#abbreviation-casing)        | 
| [No Underscores](#no-underscores)        | 
| [Type Names](#type-names)        | 
| [Implicit Types](#implicit-types)        | 
| [Noun Class Names](#noun-class-names)        | 
| [Interfaces](#interfaces)        | 
| [File Names](#file-names)        | 
| [Namespaces](#namespaces)        | 
| [Curly Brackets](#curly-brackets)        | 
| [Member Variables](#member-variables)        | 
| [Enums](#enums)        | 
| [Enum Types](#enum-types)        | 
| [Enum Suffix](#enum-suffix)        | 
| [Table Reference](#table-reference)    | 

## Class Names

**Use PascalCasing for class names and method names.**

	public class ClientActivity
	{
	    public void ClearStatistics()
	    {
	        //...
	    }
	    public void CalculateStatistics()
	    {
	        //...
	    }
	}

## Variable Names

**Use camelCasing for local variables and method arguments.**

	public class UserLog
	{
	    public void Add(LogEvent logEvent)
	    {
	        int itemCount = logEvent.Items.Count;
	        // ...
	    }
	}

## Identifiers

**Do not use Hungarian notation or any other type identification in identifiers**

	// Correct
	int counter;
	string name;
	 
	// Avoid
	int iCounter;
	string strName;

## Constants

**Do not use Screaming Caps for constants or readonly variables**

	// Correct
	public static const string ShippingType = "DropShip";
	 
	// Avoid
	public static const string SHIPPINGTYPE = "DropShip";

## Abbreviations

**Avoid using Abbreviations.**

Exceptions: abbreviations commonly used as names, such as Id, Xml, Ftp, Uri

	// Correct
	UserGroup userGroup;
	Assignment employeeAssignment;
	 
	// Avoid
	UserGroup usrGrp;
	Assignment empAssignment;
	 
	// Exceptions
	CustomerId customerId;
	XmlDocument xmlDocument;
	FtpHelper ftpHelper;
	UriPart uriPart;

## Abbreviation Casing

**Do use PascalCasing for abbreviations 3 characters or more (2 chars are both uppercase)**

	HtmlHelper htmlHelper;
	FtpTransfer ftpTransfer;
	UIControl uiControl;

## No Underscores

**Do not use Underscores in identifiers.**

Exception: you can prefix private static variables with an underscore.


	// Correct
	public DateTime clientAppointment;
	public TimeSpan timeLeft;
	 
	// Avoid
	public DateTime client_Appointment;
	public TimeSpan time_Left;
	 
	// Exception
	private DateTime _registrationDate;

## Type Names

**Do use predefined type names instead of system type names like Int16, Single, UInt64, etc**

	// Correct
	string firstName;
	int lastIndex;
	bool isSaved;
	 
	// Avoid
	String firstName;
	Int32 lastIndex;

## Implicit Types

**Do use implicit type var for local variable declarations.**

Exception: primitive types (int, string, double, etc) use predefined names.

	var stream = File.Create(path);
	var customers = new Dictionary();
	 
	// Exceptions
	int index = 100;
	string timeSheet;
	bool isCompleted;

## Noun Class Names

**Do use noun or noun phrases to name a class.**

	public class Employee
	{
	}
	public class BusinessLocation
	{
	}
	public class DocumentCollection
	{
	}

## Interfaces

**Do prefix interfaces with the letter I.  Interface names are noun (phrases) or adjectives.**

	public interface IShape
	{
	}
	public interface IShapeCollection
	{
	}
	public interface IGroupable
	{
	}

## File Names

**Do name source files according to their main classes. Exception: file names with partialclasses reflect their source or purpose, e.g. designer, generated, etc.**

	// Located in Task.cs
	public partial class Task
	{
	    //...
	}

	// Located in Task.generated.cs
	public partial class Task
	{
	    //...
	}

## Namespaces

**Do organize namespaces with a clearly defined structure**

	// Examples
	namespace Company.Product.Module.SubModule
	namespace Product.Module.Component
	namespace Product.Layer.Module.Group

## Curly Brackets

**Do vertically align curly brackets.**

	// Correct
	class Program
	{
	    static void Main(string[] args)
	    {
	    }
	}

## Member Variables

**Variables declaration**

	// Correct
	public class Account
	{
	    public static string BankName;
	    public static decimal Reserves;
	 	//Constant Fields
	 	//Fields
	    // Constructor
	    public Account()
	    {
	        // ...
	    }
	    //Methods
	    //Properties
	    public string Number {get; set;}
	    public DateTime DateOpened {get; set;}
	    public DateTime DateClosed {get; set;}
	    public decimal Balance {get; set;}
	 
	}

## Enums

**Do use singular names for enums. Exception: bit field enums.**

	// Correct
	public enum Color
	{
	    Red,
	    Green,
	    Blue,
	    Yellow,
	    Magenta,
	    Cyan
	}
	 
	// Exception
	[Flags]
	public enum Dockings
	{
	    None = 0,
	    Top = 1, 
	    Right = 2, 
	    Bottom = 4,
	    Left = 8
	}

## Enum Types

**Do notexplicitly specify a type of an enum or values of enums (except bit fields)**

	// Don't
	public enum Direction : long
	{
	    North = 1,
	    East = 2,
	    South = 3,
	    West = 4
	}
	 
	// Correct
	public enum Direction
	{
	    North,
	    East,
	    South,
	    West
	}

## Enum Suffix

**Do not suffix enum names with Enum**

	// Don't
	public enum CoinEnum
	{
	    Penny,
	    Nickel,
	    Dime,
	    Quarter,
	    Dollar
	}
	 
	// Correct
	public enum Coin
	{
	    Penny,
	    Nickel,
	    Dime,
	    Quarter,
	    Dollar
	}


## Table Reference
| Object Name               | Notation   | Length | Plural | Prefix | Suffix | Abbreviation | Char Mask          | Underscores |
|:--------------------------|:-----------|-------:|:-------|:-------|:-------|:-------------|:-------------------|:------------|
| Namespace name            | PascalCase |    128 | Yes    | Yes    | No     | No           | [A-z][0-9]         | No          |
| Class name                | PascalCase |    128 | No     | No     | Yes    | No           | [A-z][0-9]         | No          |
| Constructor name          | PascalCase |    128 | No     | No     | Yes    | No           | [A-z][0-9]         | No          |
| Method name               | PascalCase |    128 | Yes    | No     | No     | No           | [A-z][0-9]         | No          |
| Method arguments          | camelCase  |    128 | Yes    | No     | No     | Yes          | [A-z][0-9]         | No          |
| Local variables           | camelCase  |     50 | Yes    | No     | No     | Yes          | [A-z][0-9]         | No          |
| Constants name            | PascalCase |     50 | No     | No     | No     | No           | [A-z][0-9]         | No          |
| Field name                | camelCase  |     50 | Yes    | No     | No     | Yes          | [A-z][0-9]         | Yes         |
| Properties name           | PascalCase |     50 | Yes    | No     | No     | Yes          | [A-z][0-9]         | No          |
| Delegate name             | PascalCase |    128 | No     | No     | Yes    | Yes          | [A-z]              | No          |
| Enum type name            | PascalCase |    128 | Yes    | No     | No     | No           | [A-z]              | No          |

## Official Reference
1. [MSDN Coding Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)
2. [DoFactory C# Coding Standards and Naming Conventions](http://www.dofactory.com/reference/csharp-coding-standards) 
